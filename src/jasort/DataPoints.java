package jasort;

import java.awt.Color;
import java.awt.Graphics2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author john
 */
/*
 bonded pair of a data array and its sort vector.
 pairs the data with a given Sort index.
 */
public class DataPoints {
  private VectT SVect = new VectT();// sorted index
  private PListT PList = new PListT();// actual xyz data points

  // Array of 3d points to sort.
  public static class PListT extends ArrayList<PointND> {
    public int getLength() {
      return this.size();
    }

    public void Init_Random(int NumPoints, double Range) {
      this.clear();
      PointND pnt;
      for (int cnt = 0; cnt < NumPoints; cnt++) {
        pnt = new PointND();
        pnt.Randomize(Range);
        this.add(pnt);
      }
    }

    public void Normalize(CubeND minmax) {
      for (PointND pnt : this) {
        for (int cnt = 0; cnt < pnt.Loc.length; cnt++) {
          pnt.Loc[cnt] = (pnt.Loc[cnt] - minmax.Limit[0][cnt]) / minmax.Sz[cnt];
        }
      }
    }
    private void Multiply(double Range) {
      for (PointND pnt : this) {
        for (int cnt = 0; cnt < pnt.Loc.length; cnt++) {
          pnt.Loc[cnt] *= Range;
        }
      }
    }
  }

  // Sort vector (sort index) type. 
  public static class VectT {// to do: refactor this as an ArrayList
    private int PointCount;// PointCount is different from array length as it only represents the part of the array that is being used.
    private int[] IntRay;

    public int get(int dex) {
      return this.IntRay[dex];
    }
    public void set(int dex, int value) {
      this.IntRay[dex] = value;
    }
    public int GetNumPoints() {
      return this.PointCount;
    }
    void BlankVect() {
      Arrays.fill(this.IntRay, 0);
      this.PointCount = 0;
    }
    public void Append(int Value) {
      this.IntRay[this.PointCount] = Value;
      this.PointCount++;
    }
    //***********************************************************
    // Initialize a sort index (vector) to 0,1,2,3... NumElements-1.
    //********************************************************** */
    void InitForSorting(int NumElements) {
      int Cnt;
      this.IntRay = new int[NumElements];
      for (Cnt = 0; Cnt < NumElements; Cnt++) {
        this.IntRay[Cnt] = Cnt;
      }
      this.PointCount = NumElements;
    }
  }

  //************************************************************
  // Return the sort key of our data, given the sort index and 
  // "key index", IE the dimension it is sorted by. 
  //************************************************************
  double KeyOf(int Index, int Dimension) {
    int SubjIndex;/* SubjIndex is the actual data array subscript. */

    SubjIndex = this.SVect.get(Index);
    return (this.PList.get(SubjIndex).Loc[Dimension]);
  }

  public PointND get(int Dex) {
    int PntDex = this.SVect.get(Dex);
    return this.PList.get(PntDex);
  }
  public int getIndex(int Dex) {
    return this.SVect.get(Dex);
  }
  public PointND getFromIndex(int PntDex) {
    return this.PList.get(PntDex);
  }

  public int getLength() {
    return this.PList.size();
  }
  //***********************************************************
  // Swap two sort vector elements (ints).
  //***********************************************************
  void SwapIt(int FirstLoc, int SecondLoc) {
    int Temp;
    Temp = this.SVect.get(FirstLoc);
    this.SVect.set(FirstLoc, this.SVect.get(SecondLoc));
    this.SVect.set(SecondLoc, Temp);
  }

  public void Normalize(double Range) {
    CubeND minmax = new CubeND();
    minmax.SetLimits(this);
    this.PList.Normalize(minmax);
    this.PList.Multiply(Range);
  }

  public void Init_Random(int NumPoints, double Range) {
    this.PList.Init_Random(NumPoints, Range);
    this.SVect.InitForSorting(NumPoints);
  }

  /* ********************************************************************************* */
  public void ReadFile(String filename, double Range) {
    this.PList.clear();
    PointND pnt;
    String[] chunks;
    try {
      File file = new File(filename);
      Scanner reader = new Scanner(file);
      while (reader.hasNextLine()) {
        String data = reader.nextLine();
        chunks = data.split(" ");
        if (chunks.length >= 3) {// Only read lines with format 'X Y Z'
          pnt = new PointND();
          pnt.Parse(chunks[0], chunks[1], chunks[2]);
          this.PList.add(pnt);
        }
      }
      reader.close();
      this.SVect.InitForSorting(this.PList.size());
      this.Normalize(Range);
    } catch (FileNotFoundException e) {
      System.out.println("An error occurred.");
      e.printStackTrace();
    }
  }
  /* ********************************************************************************* */
  @Override public String toString() {
    StringBuilder sb = new StringBuilder();
    int NumPoints = this.SVect.GetNumPoints();
    for (int cnt = 0; cnt < NumPoints; cnt++) {
      int Dex = this.SVect.get(cnt);
      PointND pnt = this.PList.get(Dex);
      sb.append(pnt.toString() + ", \n");
    }
    return sb.toString();
  }
  /* ********************************************************************************* */
  public void Draw_Me(Graphics2D g2d) {// Just draw the points
    PointND pnt = this.get(0);
    int NumPoints = this.SVect.GetNumPoints();
    for (int cnt = 0; cnt < NumPoints; cnt++) {
      pnt = this.get(cnt);
      pnt.Draw_Me(g2d);
    }
  }
  /* ********************************************************************************* */
  public void Draw_Lines(Graphics2D g2d, Color color) {
    int xprev, yprev, xloc, yloc;
    double Factor = 1.0;
    PointND pnt = this.get(0);
    xprev = (int) (Factor * pnt.Loc[0]);
    yprev = (int) (Factor * pnt.Loc[1]);
    int NumPoints = this.SVect.GetNumPoints();
    g2d.setColor(color);
    for (int cnt = 0; cnt < NumPoints; cnt++) {
      pnt = this.get(cnt);
      xloc = (int) (Factor * pnt.Loc[0]);
      yloc = (int) (Factor * pnt.Loc[1]);
      g2d.drawLine(xprev, yprev, xloc, yloc);
      xprev = xloc;
      yprev = yloc;
    }
  }
}
