package jasort;

import java.awt.Color;
import java.awt.Graphics2D;
import static jasort.Space.MaxDims;
import java.util.Random;

/**
 *
 * @author john
 */
public class PointND {// 3d+ point type. 
  public static Random rand = new Random();
  double[] Loc = new double[MaxDims];/* xyz location */

  Color color = Color.black;
  public PointND() {
    this.color = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
  }
  public void Randomize(double Range) {
    for (int cnt = 0; cnt < MaxDims; cnt++) {
      this.Loc[cnt] = rand.nextDouble() * Range;
    }
    this.color = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
  }
  @Override public String toString() {
    StringBuilder sb = new StringBuilder();
    double Distance;
    sb.append("(");
    for (int DimCnt = 0; DimCnt < Space.SortDims; DimCnt++) {
      Distance = this.Loc[DimCnt];
      sb.append(Distance + ", ");
    }
    sb.append(")");
    return sb.toString();
  }
  public void Draw_Me(Graphics2D g2d) {
    int xloc, yloc;
    int size = 1;
    g2d.setColor(this.color);
    xloc = (int) (this.Loc[0]);
    yloc = (int) (this.Loc[1]);
    g2d.fillRect(xloc, yloc, size, size);
  }
  public void Draw_Big(Graphics2D g2d, int size) {
    int xloc, yloc;
    g2d.setColor(this.color);
    xloc = (int) (this.Loc[0]);
    yloc = (int) (this.Loc[1]);
    g2d.fillRect(xloc, yloc, size, size);
  }
  public void Draw_Big(Graphics2D g2d, int size, Color bigcolor) {
    int xloc, yloc;
    g2d.setColor(bigcolor);
    xloc = (int) (this.Loc[0]);
    yloc = (int) (this.Loc[1]);
    g2d.fillRect(xloc, yloc, size, size);
  }
  public void Parse(String xtxt, String ytxt, String ztxt) {// only 3d for this one
    this.Loc[0] = Double.parseDouble(xtxt);
    this.Loc[1] = Double.parseDouble(ytxt);
    this.Loc[2] = Double.parseDouble(ztxt);
  }
}
