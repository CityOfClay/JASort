package jasort;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.Rectangle2D;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author john
 */
/* ********************************************************************************* */
public class DrawingPanel extends JPanel implements MouseMotionListener, MouseListener, MouseWheelListener, ComponentListener, KeyListener {
  int ScreenMouseX = 0, ScreenMouseY = 0;
  double DragMouseX, DragMouseY;
  double MouseOffsetX = 0, MouseOffsetY = 0;
  Space space = new Space();
  DataPoints datapoints = new DataPoints();
  DataPoints.VectT OutRay = new DataPoints.VectT();// ArrayList would be easier but slower.
  DataPoints.VectT HitPoints = new DataPoints.VectT();
  Rectangle2D SelectBoxPrev = new Rectangle2D.Double(0, 0, 1, 1);
  Rectangle2D SelectBox = new Rectangle2D.Double(0, 0, 1, 1);
  public JFrame frame;

  /* ********************************************************************************* */
  public DrawingPanel(JFrame initFrame) {
    this.frame = initFrame;
    this.Init();
  }
  /* ********************************************************************************* */
  public final void Init() {
    this.addMouseListener(this);
    this.addMouseMotionListener(this);
    this.addMouseWheelListener(this);
    this.addKeyListener(this);
    this.addComponentListener(this);
    this.setFocusable(true);// for KeyListener
    this.requestFocusInWindow();// for KeyListener

    this.InitData();
  }
  /* ********************************************************************************* */
  public void InitData() {
    int NumPoints;
//    Various configurations to try.
//    NumPoints = 1000000;// about 1 second
//    NumPoints = 500000;// about 500 milliseconds
    NumPoints = 100000;// about 50 milliseconds, best show
//    NumPoints = 10000;// about 5 milliseconds
//    NumPoints = 1000;// about ? milliseconds
    if (false) {// enable this to read xyz points from a file
      this.datapoints.ReadFile("xyz.txt", 800.0);
      NumPoints = this.datapoints.getLength();
    } else {
      datapoints.Init_Random(NumPoints, 800.0);
    }
    this.OutRay.InitForSorting(NumPoints);
    this.HitPoints.InitForSorting(NumPoints);

    long start = System.currentTimeMillis();
    space.Jasort(datapoints);
    long finish = System.currentTimeMillis();
    long duration = finish - start;
    System.out.println("2D sort time:" + duration + " milliseconds");
    this.frame.setTitle("JASort, " + NumPoints + " points, " + Space.SortDims + "D sort time: " + duration + " milliseconds");
  }
  /* ********************************************************************************* */
  public void ClearSelectBox() {
    this.SelectBox.setFrame(0, 0, 0, 0);
    this.SelectBoxPrev.setFrame(0, 0, 0, 0);
    Graphics2D g2d = (Graphics2D) this.getGraphics();
    DrawSelectBox(g2d);
  }
  /* ********************************************************************************* */
  public void DrawResults(DataPoints.VectT vect, Graphics2D g2d, Color color, int size) {
    int NumPoints = vect.GetNumPoints();
    int Dex;
    PointND pnt;
    for (int cnt = 0; cnt < NumPoints; cnt++) {
      Dex = vect.get(cnt);
      pnt = this.datapoints.getFromIndex(Dex);
      pnt.Draw_Big(g2d, size, color);
    }
  }
  /* ********************************************************************************* */
  public void Draw_Me(Graphics2D g2d) {
    g2d.setBackground(Color.black);
    g2d.clearRect(0, 0, this.getWidth(), this.getHeight());
    datapoints.Draw_Me(g2d);
  }
  /* ********************************************************************************* */
  public void DrawSelectBox(Graphics2D g2d) {
    g2d.setXORMode(Color.white);// using white as the XOR color.
    Stroke prevstroke = g2d.getStroke();
    g2d.setStroke(new BasicStroke(3));
    g2d.setColor(Color.black);
    g2d.draw(this.SelectBoxPrev);
    g2d.setColor(Color.black);
    g2d.draw(this.SelectBox);
    g2d.setStroke(prevstroke);
  }
  /* ********************************************************************************* */
  public void DrawLines(Graphics2D g2d) {
    this.datapoints.Draw_Lines(g2d, Color.yellow);
  }
  /* ********************************************************************************* */
  @Override public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2d = (Graphics2D) g;
    System.out.println("paintComponent");
    Draw_Me(g2d);// redrawing everything is overkill for every little change or move. to do: optimize this
  }
  @Override public void update(Graphics g) {
    paint(g);// override update to prevent clearing of background
  }
  /* ********************************************************************************* */
  @Override public void mouseDragged(MouseEvent me) {
    this.DragMouseX = me.getX();
    this.DragMouseY = me.getY();
    this.SelectBoxPrev.setFrame(this.SelectBox);
    this.SelectBox.setFrame(this.ScreenMouseX, this.ScreenMouseY, DragMouseX - this.ScreenMouseX, DragMouseY - this.ScreenMouseY);
    Graphics2D g2d = (Graphics2D) this.getGraphics();
    this.DrawSelectBox(g2d);
  }
  @Override public void mouseMoved(MouseEvent me) {
    this.ScreenMouseX = me.getX();
    this.ScreenMouseY = me.getY();
  }
  @Override public void mouseClicked(MouseEvent me) {
    System.out.println("mouseClicked");
  }
  @Override public void mousePressed(MouseEvent me) {
    System.out.println("mousePressed");
    this.DragMouseX = this.ScreenMouseX = me.getX();
    this.DragMouseY = this.ScreenMouseY = me.getY();
    this.SelectBoxPrev.setFrame(this.SelectBox);
    this.SelectBox.setFrame(this.ScreenMouseX, this.ScreenMouseY, DragMouseX - this.ScreenMouseX, DragMouseY - this.ScreenMouseY);
    Graphics2D g2d = (Graphics2D) this.getGraphics();
    this.DrawSelectBox(g2d);
  }
  @Override public void mouseReleased(MouseEvent me) {
    System.out.println("mouseReleased");
    CubeND cube = new CubeND();
    cube.SetCube(this.SelectBox.getX(), this.SelectBox.getY(), 0.0, this.SelectBox.getMaxX(), this.SelectBox.getMaxY(), 1.0);
    this.OutRay.BlankVect();
    this.HitPoints.BlankVect();

    long start = System.currentTimeMillis();
    space.TraverseZone(this.datapoints, cube, this.HitPoints, this.OutRay);
    long finish = System.currentTimeMillis();
    long duration = finish - start;
    System.out.println("2D search time:" + duration + " milliseconds");
    int NumPoints = this.datapoints.getLength();
    this.frame.setTitle("JASort, " + NumPoints + " points, " + Space.SortDims + "D search time: " + duration + " milliseconds");

    Graphics2D g2d = (Graphics2D) this.getGraphics();
    this.DrawResults(this.HitPoints, g2d, Color.red, 4);
    this.DrawResults(this.OutRay, g2d, Color.red, 4);
  }
  @Override public void mouseEntered(MouseEvent me) {
    System.out.println("mouseEntered");
  }
  @Override public void mouseExited(MouseEvent me) {
    System.out.println("mouseExited");
  }
  /* ********************************************************************************* */
  @Override public void mouseWheelMoved(MouseWheelEvent mwe) {
    double XCtr, YCtr;// not used yet
    System.out.println("mouseWheelMoved");
    XCtr = mwe.getX();
    YCtr = mwe.getY();
    double finerotation = mwe.getPreciseWheelRotation();
  }
  /* ********************************************************************************* */
  @Override public void componentResized(ComponentEvent ce) {
    System.out.println("componentResized");
    this.ClearSelectBox();
  }
  @Override public void componentMoved(ComponentEvent ce) {
  }
  @Override public void componentShown(ComponentEvent ce) {
  }
  @Override public void componentHidden(ComponentEvent ce) {
  }
  /* ********************************************************************************* */
  @Override public void keyTyped(KeyEvent ke) {
    System.out.println("keyTyped:" + ke.getKeyChar());
  }
  @Override public void keyPressed(KeyEvent ke) {
    System.out.println("keyPressed:" + ke.getKeyChar());
    Graphics2D g2d = (Graphics2D) this.getGraphics();
    switch (ke.getKeyChar()) {
      case 'r': // r key regenerates new random data
        this.InitData();
        this.ClearSelectBox();
        this.repaint();
        break;
      case 'x':// x key just refreshes the image, erasing all previous graphics
        this.ClearSelectBox();
        this.repaint();
        break;
      case 'l'://  l key draws a single line through all points, showing their sort order
        this.DrawLines(g2d);
        break;
    }
  }
  @Override public void keyReleased(KeyEvent ke) {
    System.out.println("keyReleased:" + ke.getKeyChar());
  }
}
