package jasort;

import static jasort.Space.MaxDims;

/**
 *
 * @author john
 */
// N-dimensional bounding box. For more dimensions, just increase MaxDims.
public class CubeND {
  double[][] Limit = new double[2][MaxDims];// limits of volume.
  double[] Sz = new double[MaxDims];// Wdt,Hgt,Dep of space; 

  //***********************************************************
  // Set the box's Wdt,Hgt,Dep to match its limits.           *
  //***********************************************************
  public void SetCubeSz() {
    CubeND Z = this;
    int Dim;
    for (Dim = 0; Dim < MaxDims; Dim++) {
      Z.Sz[Dim] = Z.Limit[1][Dim] - Z.Limit[0][Dim];
    }
  }
  //***********************************************************
  // Set the limits on a box.                                 *
  //***********************************************************
  public void SetCube(double x1, double y1, double z1, double x2, double y2, double z2) {
    CubeND Z = this;
    Z.Limit[0][0] = x1;
    Z.Limit[0][1] = y1;
    Z.Limit[0][2] = z1;
    Z.Limit[1][0] = x2;
    Z.Limit[1][1] = y2;
    Z.Limit[1][2] = z2;
    Z.SetCubeSz();
  }

  //***********************************************************/
  // Set the box's Wdt,Hgt to match its limits.               */
  //***********************************************************/
  void SetBoxWH() {
    CubeND bx = this;
    bx.Sz[0] = bx.Limit[1][0] - bx.Limit[0][0];
    bx.Sz[1] = bx.Limit[1][1] - bx.Limit[0][1];
    /*    bx.Sz[2]=bx.L[1][2]-bx.L[0][2];  // z stuff */
  }

  //***********************************************************/
  // Set the limits on a box.                                 */
  //***********************************************************/
  void SetBox(double x1, double y1, double x2, double y2) {
    CubeND bx = this;
    /*    SetCube(x1,y1,0,x1,y1,0,bx); */
    bx.Limit[0][0] = x1;
    bx.Limit[0][1] = y1;
    bx.Limit[1][0] = x2;
    bx.Limit[1][1] = y2;
    /*    bx.L[0][2]=0; bx.L[1][2]=0; */
    bx.SetBoxWH();
  }

  //**************************************************************** 
  // Determine if a point is inside a sorted n-dimensional cube. 
  //*************************************************************** */
  boolean InSpace(PointND Point, int Dimensions) {
    byte Cnt = 0;
    while (Cnt < Dimensions) {
      if ((Point.Loc[Cnt] < this.Limit[0][Cnt]) || (this.Limit[1][Cnt] < Point.Loc[Cnt])) {
        return false;
      }
      Cnt++;
    }
    return true;
//    boolean Inside = true;
//    while ((Cnt < Dimensions) && Inside) {
//      if ((Point.Loc[Cnt] < this.L[0][Cnt]) || (this.L[1][Cnt] < Point.Loc[Cnt])) {
//        Inside = false;
//      }
//      Cnt++;
//    }
//    return (Inside);
  }

  //*************************************************************** */
  // * Fit all of BBox tightly inside ABox without distorting it.  * */
  // * Similar to SHOW command in Rocky Mountain Basic, except that* */
  // * it only works on the box data structure.  You have to set   * */
  // * the screen windows yourself with the results.               * */
  //*************************************************************** */
  void OrthoFitBox(CubeND ABox, CubeND BBox) {
    double axtoy, bxtoy;
    bxtoy = BBox.Sz[0] / BBox.Sz[1];
    axtoy = ABox.Sz[0] / ABox.Sz[1];
    if (Math.abs(bxtoy) > Math.abs(axtoy)) {// If b is wider and squatter than a.
      BBox.Limit[1][1] = BBox.Limit[0][1] + BBox.Sz[0] * Math.abs((ABox.Sz[1] / ABox.Sz[0]));
    } else {// Else if b is taller and narrower than a. 
      BBox.Limit[1][0] = BBox.Limit[0][0] + BBox.Sz[1] * Math.abs((ABox.Sz[0] / ABox.Sz[1]));
    }
    BBox.SetBoxWH();
  }

  //********************************************************************
  //  N-dimensional; flip the polarity of one dimension of the cube.
  //******************************************************************* */
  void FlipCubeDim(int dimension) {
    double temp;
    temp = this.Limit[0][dimension];
    this.Limit[0][dimension] = this.Limit[1][dimension];
    this.Limit[1][dimension] = temp;
    this.SetBoxWH();
  }

  //********************************************************************
  //   N-dimensional; set the *direction* of BBox's proportions to 
  // equal the direction of ABox's proportions.  E.G. if ABox has
  // negative width, then set BBox to have negative width; if ABox
  // has positive height, then set BBox to also have positive height.
  //   BBox's actual size and location are not changed.
  //******************************************************************* */
  void PolarizeND(CubeND BBox, int dimension) {
    double sign, swap;
    int DimCnt;
    for (DimCnt = 0; DimCnt < dimension; DimCnt++) {
      sign = this.Sz[DimCnt] * BBox.Sz[DimCnt];
      if (sign < 0) {// if signs are different, invert BBox limits. 
        swap = BBox.Limit[0][DimCnt];
        BBox.Limit[0][DimCnt] = BBox.Limit[1][DimCnt];
        BBox.Limit[1][DimCnt] = swap;
      }
    }
    BBox.SetCubeSz();
  }

  //***********************************************************************
  //   N-dimensional; set BBox's proportions to equal ABox's proportions, 
  // while keeping all of old BBox's contents tightly inside the new BBox.
  //***********************************************************************
  void OrthoFitND(CubeND BBox, int dimension) {
    double MinRat, Center, HalfSz;
    double[] Ratio = new double[MaxDims];
    int DimCnt;
    MinRat = Space.maxint;// absurd value to lose first comparison. 

    for (DimCnt = 0; DimCnt < dimension; DimCnt++) {// first get ratios of sizes, and narrowest dimension.

      Ratio[DimCnt] = Math.abs(this.Sz[DimCnt] / BBox.Sz[DimCnt]);
      if (Ratio[DimCnt] < MinRat) {
        MinRat = Ratio[DimCnt];
      }
    }
    for (DimCnt = 0; DimCnt < dimension; DimCnt++) {
      BBox.Sz[DimCnt] = (BBox.Sz[DimCnt]) * (Ratio[DimCnt] / MinRat);
      Center = (BBox.Limit[1][DimCnt] + BBox.Limit[0][DimCnt]) / 2.0;
      HalfSz = BBox.Sz[DimCnt] / 2;
      BBox.Limit[0][DimCnt] = Center - HalfSz;
      BBox.Limit[1][DimCnt] = Center + HalfSz;
    }
    BBox.SetCubeSz();
  }
  //*********************************************************
  // get spatial bounds of data and set them as min, max of this cube
  //*********************************************************
  void SetLimits(DataPoints sp) {
    int Cnt, NumPoints;
    byte DimCnt;
    NumPoints = sp.getLength();
    if (NumPoints > 0) {// set initial bounds to ridiculous inverted extremes.
      for (DimCnt = 0; DimCnt < MaxDims; DimCnt++) {
        this.Limit[0][DimCnt] = Space.maxint;
        this.Limit[1][DimCnt] = -Space.maxint;
      }
      for (Cnt = 0; Cnt < NumPoints; Cnt++) {
        for (DimCnt = 0; DimCnt < MaxDims; DimCnt++) {
          // This is important, before sorting your data test its min, max bounds 
          // and keep them in MinMax[0] and MinMax[1]. 
          if (sp.get(Cnt).Loc[DimCnt] < this.Limit[0][DimCnt]) {
            this.Limit[0][DimCnt] = sp.get(Cnt).Loc[DimCnt];
          }
          if (sp.get(Cnt).Loc[DimCnt] > this.Limit[1][DimCnt]) {
            this.Limit[1][DimCnt] = sp.get(Cnt).Loc[DimCnt];
          }
        }
      }
    } else {// if no data, set MinMax to nearly zero.
      for (DimCnt = 0; DimCnt < MaxDims; DimCnt++) {
        this.Limit[0][DimCnt] = 0;
        this.Limit[1][DimCnt] = Space.fudge;
      }
    }
    this.SetCubeSz();// set MinMax's size to match its bounds. 
  }

}
