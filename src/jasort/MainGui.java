package jasort;

import java.awt.Container;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;

/**
 * @author MultiTool
 */
public class MainGui {
  JFrame frame;
  DrawingPanel drawpanel;
  /* ********************************************************************************* */
  public MainGui() {
  }
  /* ********************************************************************************* */
  public void Init() {
    this.frame = new JFrame();
    this.frame.setTitle("JASort");
    this.frame.setSize(825, 850);
    this.frame.addWindowListener(new WindowAdapter() {
      @Override public void windowClosing(WindowEvent e) {
        System.exit(0);
      }
    });
    Container contentPane = this.frame.getContentPane();
    this.drawpanel = new DrawingPanel(this.frame);
    contentPane.add(this.drawpanel);
    frame.setVisible(true);
  }
}
