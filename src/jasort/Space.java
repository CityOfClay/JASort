package jasort;

class Space {
  // This was ported from C language recently, pardon our dust.
  public static int maxint = Integer.MAX_VALUE;
  public static double fudge = Double.MIN_VALUE;
  public static int MaxDims = 3;// even if we are sorting by 1D or 2D, points can still be 3D.
//  public static int SortDims = 1;// sort by 1 dimension. (just X) 
  public static int SortDims = 2;// sort by 2 dimensions. (XY) 

  //***********************************************************
  // Sorts a section of the point array into < or > pivotval.    
  //************************************************************
  int Partition(DataPoints sp, int Low, int High, int PivotLoc, int Dimension) {
    double PivotVal;
    int DataIndex, LastSmall;

    sp.SwapIt(Low, (Low + High + 1) >> 1);// shift right 1 is same as div 2 but faster.

    PivotVal = sp.KeyOf(Low, Dimension);
    LastSmall = Low;
    for (DataIndex = Low + 1; DataIndex <= High; DataIndex++) {
      if (sp.KeyOf(DataIndex, Dimension) < PivotVal) {
        LastSmall++;
        sp.SwapIt(LastSmall, DataIndex);
      }
    }
    sp.SwapIt(Low, LastSmall);
    PivotLoc = LastSmall;
    return PivotLoc;
  }

  //*********************************************************
  // Partition vector right down the population middle, or 
  // statistical median.  Used for spatial plane sort.
  //**********************************************************
  int MedioPartition(DataPoints sp, int MLow, int MHigh, int MedianLoc, int Dimension) {
    int PivotLoc = -1;
    MedianLoc = (MLow + MHigh) >> 1;// shift right 1 is same as div 2 but faster.
    while (PivotLoc != MedianLoc) {
      PivotLoc = Partition(sp, MLow, MHigh, PivotLoc, Dimension);
      if (MedianLoc < PivotLoc) {// Partition first half, on next iteration.
        MHigh = PivotLoc - 1;
      } else {// Partition second half, on next iteration.
        MLow = PivotLoc + 1;
      }
    }
    // Now the median is found, and we are partitioned on the median.
    return MedianLoc;
  }

  //*********************************************************
  //  Recursive part, most closely resembles quicksort here,
  // except for balanced partitioning.
  //**********************************************************
  void JasortRecurse(DataPoints sp, int Low, int High, int SortLevel) {
    int MedianLoc = 0;
    if (Low < High) {
      MedianLoc = MedioPartition(sp, Low, High, MedianLoc, SortLevel % SortDims);
      SortLevel++;
      JasortRecurse(sp, Low, MedianLoc - 1, SortLevel);
      JasortRecurse(sp, MedianLoc + 1, High, SortLevel);
    }
  }

  //*********************************************************
  // Sorts multidimensionally.
  //**********************************************************
  void Jasort(DataPoints sp) {
    JasortRecurse(sp, 0, sp.getLength() - 1, 0);
  }

  //**************************************************************
  // Recursive traverse of a "SortDims"-dimensional sorted region.
  // All points found are returned indexed in OutRay.
  //  Use TraverseZone as front end to this.
  //************************************************************** */
  void Treeverse(DataPoints dpoints, CubeND Zone, int MinLoc, int MaxLoc, int Level, DataPoints.VectT HitPoints, DataPoints.VectT OutRay) {
    PointND Pnt;
    int MedLoc, SubjIndex, LevelMod = Level % SortDims;

    MedLoc = (MinLoc + MaxLoc) >> 1;// shift right 1 is same as div 2 but faster.

    // get data index from sort pointer.
    SubjIndex = dpoints.getIndex(MedLoc);// median point is sample.
    Pnt = dpoints.getFromIndex(SubjIndex);// with-do for sample point.
    HitPoints.Append(SubjIndex);// HitPoints is for proof-of-concept only, do not use in production

    if ((Pnt.Loc[LevelMod] >= Zone.Limit[0][LevelMod]) && (MinLoc < MedLoc)) {
      Treeverse(dpoints, Zone, MinLoc, MedLoc - 1, Level + 1, HitPoints, OutRay);// Explore lower half.
    }
    if (Zone.InSpace(Pnt, SortDims)) {// if in zone, add to output list.
      OutRay.Append(SubjIndex);
    }
    if ((Pnt.Loc[LevelMod] <= Zone.Limit[1][LevelMod]) && (MedLoc < MaxLoc)) {
      Treeverse(dpoints, Zone, MedLoc + 1, MaxLoc, Level + 1, HitPoints, OutRay);// Explore upper half.
    }
  }
  //**************************************************************
  //  Non-recursive front end to Treeverse.
  // All points found are returned indexed in OutRay.
  //************************************************************** */
  void TraverseZone(DataPoints dpoints, CubeND Zone, DataPoints.VectT HitPoints, DataPoints.VectT OutRay) {
    int NumPoints = dpoints.getLength();
    if (NumPoints > 0) {
      Treeverse(dpoints, Zone, 0, NumPoints - 1, 0, HitPoints, OutRay);
    } else {
      OutRay.BlankVect();
    }
  }

}
